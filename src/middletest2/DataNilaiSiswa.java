package middletest2;
import java.util.Scanner;

public class DataNilaiSiswa {

    public static void main(String[] args) {
     Scanner masukan = new Scanner(System.in);
     System.out.println("Orang yang berhak melihat rapot siswa: ");
     System.out.println("1. Kepala Sekolah");
     System.out.println("2. Guru");
     System.out.println("3. Orang tua");
     System.out.println("4. Siswa");
     System.out.print("Masukkan nomor pilihan anda: ");
     int pilih = masukan.nextInt();
    
     //KEPALA SEKOLAH
     if(pilih == 1){
         System.out.println("Selamat Datang Di Data Nilai Siawa");
         System.out.println("Pilihan Untuk Anda");
         System.out.println("1. Lihat Data Nilai Siswa");
         System.out.println("2. Masukkan Nilai Pada Data Nilai Siswa");
         System.out.print("Masukkan Nomor Pilihan Anda : ");
         int pilih2 = masukan.nextInt();
         if (pilih2 == 1){
             System.out.println("Data Nilai Siswa");
             String nama1 = "Kim Taehyung";
             System.out.println("A. Nama : " + nama1);
             double matematika = 75;
             double ipa = 90;
             double bahasaIndo = 85;
             double bahasaIng = 80;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika);
             System.out.println("2. IPA : " + ipa);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo);
             System.out.println("4. Bahasa Inggris : " + bahasaIng);
             System.out.println("");
             String nama2 = "Haruno Sakura";
             System.out.println("B. Nama : " + nama2);
             double matematika2 = 80;
             double ipa2 = 91;
             double bahasaIndo2 = 75;
             double bahasaIng2 = 87;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika2);
             System.out.println("2. IPA : " + ipa2);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
             System.out.println("4. Bahasa Inggris : " + bahasaIng2);
             double ratarata = (matematika + matematika2 + ipa + ipa2 + bahasaIndo + bahasaIndo2 + bahasaIng + bahasaIng2) / 8;
             System.out.println("Rata-Rata Siswa : " + ratarata);
             }
         if(pilih2 == 2){
             String nama3;
             System.out.print("Masukkan Nama Siswa : ");
             nama3 = masukan.next();
             System.out.print("Masukan nilai Matematika: ");
             double matematika3 = masukan.nextDouble();
             System.out.print("Masukan nilai IPA: ");
             double ipa3 = masukan.nextDouble();
             System.out.print("Masukan nilai Bahasa Indonesia: ");
             double bahasaIndo3 = masukan.nextDouble();
             System.out.print("Masukan nilai Bahasa Inggris: ");
             double bahasaIng3 = masukan.nextDouble();
             System.out.println("Nama Siswa = "+ nama3);
             System.out.println("Nilai Matematika = "+ matematika3);
             System.out.println("Nilai IPA = "+ ipa3);
             System.out.println("Nilai Bahasa Indonesia = "+ bahasaIndo3);
             System.out.println("Nilai Bahasa Inggris = "+ bahasaIng3);
             double rata2 = (matematika3 + ipa3 + bahasaIndo3 + bahasaIng3)/4;
             System.out.println("Rata-Rata : " + rata2);
             }
     }
         
         //GURU
         if(pilih == 2) {
             System.out.println("Selamat Datang Di Data Nilai Siawa");
             System.out.println("Pilihan Untuk Anda");
             System.out.println("1. Lihat Data Nilai Siswa");
             System.out.println("2. Masukkan Nilai Pada Data Nilai Siswa");
             System.out.print("Masukkan Nomor Pilihan Anda : ");
             int pilih3 = masukan.nextInt();
             if (pilih3 == 1){
             System.out.println("Data Nilai Siswa");
             String nama1 = "Kim Taehyung";
             System.out.println("A. Nama : " + nama1);
             double matematika = 75;
             double ipa = 90;
             double bahasaIndo = 85;
             double bahasaIng = 80;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika);
             System.out.println("2. IPA : " + ipa);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo);
             System.out.println("4. Bahasa Inggris : " + bahasaIng);
             System.out.println("");
             String nama2 = "Haruno Sakura";
             System.out.println("B. Nama : " + nama2);
             double matematika2 = 80;
             double ipa2 = 91;
             double bahasaIndo2 = 75;
             double bahasaIng2 = 87;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika2);
             System.out.println("2. IPA : " + ipa2);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
             System.out.println("4. Bahasa Inggris : " + bahasaIng2);
             double ratarata = (matematika + matematika2 + ipa + ipa2 + bahasaIndo + bahasaIndo2 + bahasaIng + bahasaIng2) / 8;
             System.out.println("Rata-Rata Siswa : " + ratarata);
             }
         if(pilih3 == 2){
             String nama3;
             System.out.print("Masukkan Nama Siswa : ");
             nama3 = masukan.next();
             System.out.print("Masukan nilai Matematika: ");
             double matematika3 = masukan.nextDouble();    
             System.out.print("Masukan nilai IPA: ");
             double ipa3 = masukan.nextDouble();
             System.out.print("Masukan nilai Bahasa Indonesia: ");
             double bahasaIndo3 = masukan.nextDouble();
             System.out.print("Masukan nilai Bahasa Inggris: ");
             double bahasaIng3 = masukan.nextDouble();
             System.out.println("Nama Siswa = "+ nama3);
             System.out.println("Nilai Matematika = "+ matematika3);
             System.out.println("Nilai IPA = "+ ipa3);
             System.out.println("Nilai Bahasa Indonesia = "+ bahasaIndo3);
             System.out.println("Nilai Bahasa Inggris = "+ bahasaIng3);
             double rata2 = (matematika3 + ipa3 + bahasaIndo3 + bahasaIng3)/4;
             System.out.println("Rata-Rata : " + rata2);
         }
        
     }
         
         //ORANG TUA
    if (pilih == 3){
         System.out.println("Selamat Datang Di Data Nilai Siswa");
         System.out.println("Apakah anda ingin melihat nilai siswa? Ya=1 Tidak=2");
         System.out.print("Pilih nomor: ");
         int pilih4 = masukan.nextInt();
    if (pilih4 == 1){
        System.out.println("Data Nilai Siswa");
             String nama1 = "Kim Taehyung";
             System.out.println("A. Nama : " + nama1);
             double matematika = 75;
             double ipa = 90;
             double bahasaIndo = 85;
             double bahasaIng = 80;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika);
             System.out.println("2. IPA : " + ipa);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo);
             System.out.println("4. Bahasa Inggris : " + bahasaIng);
             System.out.println("");
             String nama2 = "Haruno Sakura";
             System.out.println("B. Nama : " + nama2);
             double matematika2 = 80;
             double ipa2 = 91;
             double bahasaIndo2 = 75;
             double bahasaIng2 = 87;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika2);
             System.out.println("2. IPA : " + ipa2);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
             System.out.println("4. Bahasa Inggris : " + bahasaIng2);
             double ratarata = (matematika + matematika2 + ipa + ipa2 + bahasaIndo + bahasaIndo2 + bahasaIng + bahasaIng2) / 8;
             System.out.println("Rata-Rata Siswa : " + ratarata);
             
    }
    if(pilih4 == 2){
        System.out.println("Selesai, Sampai jumpa kembali");
    }
    }
    // SISWA
    if (pilih == 4){
        System.out.println("Selamat Datang Di Data Nilai Siswa");
         System.out.println("Apakah anda ingin melihat nilai siswa? Ya=1 Tidak=2");
         System.out.print("Pilih nomor: ");
         int pilih5 = masukan.nextInt();
    if (pilih5 == 1){
        System.out.println("Data Nilai Siswa");
             String nama1 = "Kim Taehyung";
             System.out.println("A. Nama : " + nama1);
             double matematika = 75;
             double ipa = 90;
             double bahasaIndo = 85;
             double bahasaIng = 80;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika);
             System.out.println("2. IPA : " + ipa);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo);
             System.out.println("4. Bahasa Inggris : " + bahasaIng);
             System.out.println("");
             String nama2 = "Haruno Sakura";
             System.out.println("B. Nama : " + nama2);
             double matematika2 = 80;
             double ipa2 = 91;
             double bahasaIndo2 = 75;
             double bahasaIng2 = 87;
             System.out.println("NILAI :");
             System.out.println("1. Matematika : " + matematika2);
             System.out.println("2. IPA : " + ipa2);
             System.out.println("3. Bahasa Indonesia : " + bahasaIndo2);
             System.out.println("4. Bahasa Inggris : " + bahasaIng2);
             double ratarata = (matematika + matematika2 + ipa + ipa2 + bahasaIndo + bahasaIndo2 + bahasaIng + bahasaIng2) / 8;
             System.out.println("Rata-Rata Siswa : " + ratarata);
             
    }
    if(pilih5 == 2){
        System.out.println("Selesai, Sampai jumpa kembali");
    }
    }
    }
    }